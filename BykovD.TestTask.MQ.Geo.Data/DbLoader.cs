﻿using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Models;
using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Services;
using Microsoft.Extensions.Configuration;
using System.Configuration;
using System.Text;

namespace BykovD.TestTask.MQ.Geo.Data
{
    public class DbLoader : IDbLoader
    {
        string dbPath;
        Task<DbModel> taskModel;

        public DbLoader(IConfigurationRoot configurationRoot)
        {
            dbPath = configurationRoot.GetSection("DB:Path").Value;

            if (string.IsNullOrEmpty(dbPath))
                throw new ConfigurationErrorsException("Db file path isn't set in configuration.");

            // Запускаем загрузку в отдельном потоке, чтобы не тормозить старт плиложения
            taskModel = Task.Run(() => Load());
        }

        private DbModel Load()
        {
            using var br = new BinaryReader(File.Open(dbPath, FileMode.Open, FileAccess.Read, FileShare.Read), Encoding.UTF8, false);
            var dbInfo = ReadDbInfo(br);

            return new DbModel() {
                DbInfo = dbInfo,
                IpRangesBytes = br.ReadBytes(dbInfo.Records * 12),
                LocationBytes = br.ReadBytes(dbInfo.Records * 96),
                NameIndexes = br.ReadBytes(dbInfo.Records * 4)
            };
        }

        private DbInfo ReadDbInfo(BinaryReader br)
        {
            var dbInfo = new DbInfo()
            {
                Version = br.ReadInt32(),
                Name = ReadString(br, 32),
                Timestamp = br.ReadUInt64(),
                Records = br.ReadInt32(),
                OffsetRanges = br.ReadUInt32(),
                OffsetCities = br.ReadUInt32(),
                OffsetLocations = br.ReadUInt32()
            };

            return dbInfo;
        }

        private string ReadString(BinaryReader br, int length)
        {
            Span<byte> stackSpan = stackalloc byte[length];
            br.Read(stackSpan);
            return Encoding.UTF8.GetString(stackSpan).TrimEnd('\0');
        }

        public Task<DbModel> GetDbModel() => taskModel;
    }
}

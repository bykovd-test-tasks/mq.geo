﻿using BykovD.TestTask.MQ.Geo.Domain;
using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Models;
using BykovD.TestTask.MQ.Geo.Domain.Searchers;
using BykovD.TestTask.MQ.Geo.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace BykovD.TestTask.MQ.Geo.Web
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class IpController : ControllerBase
    {
        protected IpSearcher ipSearcher;
        protected LocationByCitySearcher locationByCitySearcher;

        public IpController(IpSearcher ipSearcher, LocationByCitySearcher locationByCitySearcher)
        {
            this.ipSearcher = ipSearcher;
            this.locationByCitySearcher = locationByCitySearcher;
        }

        [HttpGet()]
        public async Task<ActionResult<Location>> LocationAsync(string ip)
        {
            var ipRange = await ipSearcher.FindIpRange(ip);

            if (ipRange == null)
                return NotFound();

            var location = await locationByCitySearcher.GetLocation((int)ipRange.Value.LocationIndex);

            return Ok(new LocationInfoWithStrings(location));
        }
    }
}

﻿using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Models;
using BykovD.TestTask.MQ.Geo.Domain.Searchers;
using BykovD.TestTask.MQ.Geo.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace BykovD.TestTask.MQ.Geo.Web
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class CityController : ControllerBase
    {
        protected LocationByCitySearcher locationByCitySearcher;

        public CityController(LocationByCitySearcher locationByCitySearcher)
        {
            this.locationByCitySearcher = locationByCitySearcher;
        }

        [HttpGet()]
        public async Task<ActionResult<Location>> LocationsAsync(string city)
        {
            var locationRes = await locationByCitySearcher.FindLocations(city);

            if (locationRes.Length == 0)
                return NotFound();

            var locations = new LocationInfoWithStrings[locationRes.Length];

            for (int i = 0; i < locations.Length; i++)
            {
                var location = locationRes[i];

                locations[i] = new LocationInfoWithStrings(location);
            }

            return Ok(locations);
        }
    }
}

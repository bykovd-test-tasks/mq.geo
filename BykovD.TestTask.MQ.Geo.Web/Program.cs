using BykovD.TestTask.MQ.Geo.Data;
using BykovD.TestTask.MQ.Geo.Domain;
using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Services;
using BykovD.TestTask.MQ.Geo.Domain.Searchers;

var builder = WebApplication.CreateBuilder(args);
builder.Services
    .AddSingleton<LocationByCitySearcher>()
    .AddSingleton<IpSearcher>()
    .AddSingleton<IDbLoader>(new DbLoader(builder.Configuration))
    .AddControllers();

var app = builder.Build();

app.UseFileServer();
app.MapControllers();

app.Run();

﻿function handleClick() {
    document.getElementById('ipContent').hidden = !document.getElementById('ipContent').hidden;
    document.getElementById('cityContent').hidden = !document.getElementById('cityContent').hidden;
}


class Shared {
    showError(error) {
        alert("ERROR!!!!: " + error);
    }

    createLocationView(location) {

        var table = document.createElement("table");

        for (var prop in location) {

            var row = table.insertRow(table.rows.length)

            var nameCell = row.insertCell(row.cells.length);

            nameCell.innerHTML = prop;

            var valCell = row.insertCell(row.cells.length);

            valCell.innerHTML = location[prop];
        }

        return table;
    }
}

class IpValidation {
    #regex = RegExp("^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");

    validate() {
        var ip = document.getElementById("ip_input").value;
        var validationMessage = document.getElementById("ip_input_validation_message")

        if (this.#regex.test(ip)) {
            validationMessage.setAttribute("hidden", "hidden");
            return true;
        } else {
            validationMessage.removeAttribute("hidden");
            return false;
        }
    }
}

class IpContent {
    #shared;
    #ipValidation;

    constructor(shared, ipValidation) {
        this.#shared = shared;
        this.#ipValidation = ipValidation;
    }

    sendIp() {
        if (!this.#ipValidation.validate())
            return;

        var send_ip_button = document.getElementById('send_ip_button');
        send_ip_button.setAttribute("disabled", "disabled");
        var ip = document.getElementById('ip_input').value;

        var url = "ip/location";
        var params = "ip=" + ip;
        var http = new XMLHttpRequest();

        var _this = this;

        http.open("GET", url + "?" + params, true);
        http.onreadystatechange = function () {
            if (http.readyState == 4) {
                send_ip_button.removeAttribute("disabled");

                switch (http.status) {
                    case 0:
                        _this.#shared.showError("Server doesn't respond.");
                        break;
                    case 200:
                        _this.#renderLocation(ip, http.responseText);
                        break;
                    case 404:
                        _this.#renderLocationNotFound(ip);
                        break;
                    default:
                        _this.#shared.showError(http.responseText);
                }
            }
        }

        http.send(null);
    }

    #renderLocationNotFound(ip) {
        var ipLocation = document.getElementById("ipLocation");

        ipLocation.innerHTML = 'Location not found for \'' + ip + '\'.';
    }

    #renderLocation(ip, responseText) {
        var location = JSON.parse(responseText);

        var locationView = this.#shared.createLocationView(location);

        var ipLocations = document.getElementById("ipLocation");

        ipLocations.innerHTML = '<b>Location found for \'' + ip + '\'.</b>';

        ipLocations.appendChild(locationView);
    }
}

class CityValidation {
    validate() {
        var city = document.getElementById("city_input").value;

        var validationMessage = document.getElementById("city_input_validation_message")

        if (city.length >= 5) {
            validationMessage.setAttribute("hidden", "hidden");
            return true;
        } else {
            validationMessage.removeAttribute("hidden");
            return false;
        }
    }
}

class CityContent {
    #shared;
    #cityValidation;

    constructor(shared, cityValidation) {
        this.#shared = shared;
        this.#cityValidation = cityValidation;
    }

    sendCity() {
        if (!this.#cityValidation.validate())
            return;

        var send_city_button = document.getElementById('send_city_button');
        send_city_button.setAttribute("disabled", "disabled");
        var city = document.getElementById('city_input').value;

        var url = "city/locations";
        var params = "city=" + encodeURI(city);
        var http = new XMLHttpRequest();



        var _this = this;

        http.open("GET", url + "?" + params, true);
        http.onreadystatechange = function () {
            if (http.readyState == 4) {
                send_city_button.removeAttribute("disabled");

                switch (http.status) {
                    case 0:
                        _this.#shared.showError("Server doesn't respond.");
                        break;
                    case 200:
                        _this.#renderLocations(city, http.responseText);
                        break;
                    case 404:
                        _this.#renderLocationsNotFound(city);
                        break;
                    default:
                        _this.#shared.showError(http.responseText);
                }
            }
        }

        http.send(null);
    }

    #renderLocationsNotFound(city) {
        var cityLocations = document.getElementById("cityLocations");

        cityLocations.innerHTML = 'Locations not found for city \'' + city + '\'.';
    }

    #renderLocations(city, responseText) {
        var locations = JSON.parse(responseText);
        var cityLocations = document.getElementById("cityLocations");

        if (locations.length == 0) {
            this.#renderLocationsNotFound(city);
        }
        else {
            cityLocations.innerHTML = '<b>For city \'' + city + '\' were found ' + locations.length + ' locations.</b>';

            locations.forEach(location => {
                var locationView = this.#shared.createLocationView(location);

                cityLocations.appendChild(locationView);
            });
        }
    }
}


var shared = new Shared();

var ipValidation = new IpValidation();
var ipContent = new IpContent(shared, ipValidation);

var cityValidation = new CityValidation();
var cityContent = new CityContent(shared, cityValidation);

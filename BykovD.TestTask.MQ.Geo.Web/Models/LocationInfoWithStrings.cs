﻿using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Models;
using System.Text;

namespace BykovD.TestTask.MQ.Geo.Web.Models
{
    public struct LocationInfoWithStrings
    {
        /// <summary>
        /// название страны (случайная строка с префиксом "cou _")
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// название области (случайная строка с префиксом "rec g_")
        /// </summary>
        public string Region { get; set; }

        /// <summary>
        /// почтовый индекс (случайная строка с префиксом "pos _")
        /// </summary>
        public string Postal { get; set; }

        /// <summary>
        /// название города (случайная строка с префиксом "cit _")
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// название организации (случайная строка с префиксом blic u"org_")
        /// </summary>
        public string Organization { get; set; }

        /// <summary>
        /// широта
        /// </summary>
        public float Latitude { get; set; }

        /// <summary>
        /// долгота
        /// </summary>
        public float Longitude { get; set; }

        public LocationInfoWithStrings(Location location)
        {
            unsafe
            {
                Country = Encoding.UTF8.GetString(new ReadOnlySpan<byte>(location.Country, 8)).TrimEnd('\0');
                Region = Encoding.UTF8.GetString(new ReadOnlySpan<byte>(location.Region, 12)).TrimEnd('\0');
                Postal = Encoding.UTF8.GetString(new ReadOnlySpan<byte>(location.Postal, 12)).TrimEnd('\0');
                City = Encoding.UTF8.GetString(new ReadOnlySpan<byte>(location.City, 24)).TrimEnd('\0');
                Organization = Encoding.UTF8.GetString(new ReadOnlySpan<byte>(location.Organization, 32)).TrimEnd('\0');
                Latitude = location.Latitude;
                Longitude = location.Longitude;
            }
        }
    }
}

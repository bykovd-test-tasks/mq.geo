﻿using BykovD.TestTask.MQ.Geo.Data;
using BykovD.TestTask.MQ.Geo.Domain;
using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Models;
using BykovD.TestTask.MQ.Geo.Tests.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace BykovD.TestTask.MQ.Geo.Tests.DataTests
{
    public class DbLoader_Tests
    {
        private readonly ITestOutputHelper output;

        public DbLoader_Tests(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public async Task DbLoader_Should_Load_DbInfo_CorrectlyAsync()
        {
            var dbModel = await DbHelper.GetDbModelAsync();

            Assert.NotNull(dbModel);
            Assert.NotNull(dbModel.DbInfo);
            Assert.Equal("Geo.IP", dbModel.DbInfo.Name);
        }

         [Fact]
        public void DbLoader_Should_Take_Less_30ms()
        {
            var count = 100;
            long summ = 0;
            long max = 0;
            var configuration = DbHelper.GetConfiguration();
            var watch = new Stopwatch();

            for (int i = 0; i < count; i++)
            {
                GC.Collect(2, GCCollectionMode.Forced, true, true);
                
                watch.Restart();

                var dbLoader = new DbLoader(configuration);
                    
                var dbModel = dbLoader.GetDbModel().Result;

                watch.Stop();

                if (watch.ElapsedMilliseconds > max)
                    max = watch.ElapsedMilliseconds;

                summ += watch.ElapsedMilliseconds;
            }

            output.WriteLine($"Max: '{max}', Avg: {summ / count}");
        }

        [Fact]
        public void PrintIpRanges()
        {
            var dbModel = DbHelper.GetDbModel();

            var span = MemoryMarshal.Cast<byte, IpRange>(dbModel.IpRangesBytes);

            foreach (var ipRange in span)
            {
                output.WriteLine($"({IpHelpers.ConvertFromIntegerToIpAddress(ipRange.IpFrom)}, {IpHelpers.ConvertFromIntegerToIpAddress(ipRange.IpTo)}) - {ipRange.LocationIndex}");
            }
        }

        [Fact]
        public void PrintLocations()
        {
            var dbModel = DbHelper.GetDbModel();

            var span = MemoryMarshal.Cast<byte, Location>(dbModel.LocationBytes);

            var i = 0;
            foreach (var location in span)
            {
                unsafe
                {
                    output.WriteLine($"{i}: {Encoding.ASCII.GetString(new ReadOnlySpan<byte>(location.City, 24))}, {Encoding.ASCII.GetString(new ReadOnlySpan<byte>(location.Region, 12))}");
                }
                i++;
            }
        }

        [Fact]
        public void PrintNameIndexes()
        {
            var dbModel = DbHelper.GetDbModel();

            var span = MemoryMarshal.Cast<byte, int>(dbModel.NameIndexes);

            var array = span.ToArray();

            output.WriteLine($"Min: {array.Min(a => a)}");
            output.WriteLine($"Max: {array.Max(a => a)}");

            var first = array[0];

            output.WriteLine($"Min diff: {array.Where(a => a != first).Min(a => Math.Abs(a - first))}");

            for (var i = 0; i < span.Length; i++)
            {
                output.WriteLine($"{i}: {span[i]}");
            }
        }

        [Fact]
        public void PrintCityNameDublicatesCount()
        {
            var dbModel = DbHelper.GetDbModel();

            Location loc;
            var span = MemoryMarshal.Cast<byte, Location>(dbModel.LocationBytes);

            var dict = DbHelper.GetCityNameToCountDict(span);

            var min = dict.Values.Min();
            var max = dict.Values.Max();

            output.WriteLine($"Min: {min}");
            output.WriteLine($"Max: {max}");

            foreach (var str in dict.OrderByDescending(p => p.Value).Select(p => p.Key))
            {
                var val = dict[str];

                if (val > 1)
                    output.WriteLine($"{ str}\t{val}");
            }
        }

        [Fact]
        public void GenerateIpList()
        {
            using var fw = File.CreateText("IpList.txt");
            for (var i = 0; i < 100000; i++)
            {
                var ip = IpHelpers.GenerateIp();

                fw.WriteLine(ip);
            }
        }

        [Fact]
        public void GenerateCityNameList()
        {
            var dbModel = DbHelper.GetDbModel();

            Location loc;
            using var fw = File.CreateText("CityNames.txt");

            var span = MemoryMarshal.Cast<byte, Location>(dbModel.LocationBytes);

            unsafe
            {
                for (var i = 0; i < span.Length; i++)
                {
                    loc = span[i];

                    var str = Encoding.UTF8.GetString(new ReadOnlySpan<byte>(loc.City, 24)).TrimEnd('\0');

                    fw.WriteLine(str);
                }
            }
        }
    }
}

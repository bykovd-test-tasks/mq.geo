﻿using BykovD.TestTask.MQ.Geo.Data;
using BykovD.TestTask.MQ.Geo.Domain;
using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Models;
using BykovD.TestTask.MQ.Geo.Domain.Searchers;
using BykovD.TestTask.MQ.Geo.Tests.Helpers;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BykovD.TestTask.MQ.Geo.Tests.DataTests
{
    public class SearchService_Tests
    {
        [Fact]
        public void Check_Counts_For_Existing_()
        {
            var config = DbHelper.GetConfiguration();
            var dbModel = DbHelper.GetDbModel();

            unsafe
            {
                var span = MemoryMarshal.Cast<byte, Location>(dbModel.LocationBytes);

                var dict = DbHelper.GetCityNameToCountDict(span);

                var searchService = new LocationByCitySearcher(new DbLoader(config));

                foreach (var key in dict.Keys)
                {
                    var locations = searchService.FindLocations(key.Trim('\0')).Result;

                    Assert.NotNull(locations);

                    locations.Should().NotBeNullOrEmpty(key);

                    locations.Should().HaveCount(dict[key], key);

                    foreach (var location in locations)
                    {
                        var locCityName = Encoding.UTF8
                            .GetString(new ReadOnlySpan<byte>(location.City, 24))
                            .TrimEnd('\0');

                        locCityName.Should().Be(locCityName);
                    }
                }
            }
        }

        [Fact]
        public async Task Check_Generated_Ip_ListAsync()
        {
            var config = DbHelper.GetConfiguration();

            var searchService = new IpSearcher(new DbLoader(config));

            for (int i = 0; i < 10_000_000; i++)
            {
                var ipStr = IpHelpers.GenerateIp();

                var ipRange = await searchService.FindIpRange(ipStr);

                if(ipRange != null)
                {
                    var intIp = IpHelpers.ConvertFromIpAddressToInteger(ipStr);

                    intIp.Should().BeGreaterThanOrEqualTo(ipRange.Value.IpFrom,
                        $"actual {ipStr} less than {IpHelpers.ConvertFromIntegerToIpAddress(ipRange.Value.IpFrom)}");

                    intIp.Should().BeLessThanOrEqualTo(ipRange.Value.IpTo,
                        $"actual {ipStr} greater than {IpHelpers.ConvertFromIntegerToIpAddress(ipRange.Value.IpTo)}");
                }
            }
        }

        [Theory]
        [InlineData("6.97.103.125")]
        public async Task Check_Ip_Search(string ipStr)
        {
            var config = DbHelper.GetConfiguration();

            var searchService = new IpSearcher(new DbLoader(config));

            var ipRange = await searchService.FindIpRange(ipStr);

            if (ipRange != null)
            {
                var intIp = IpHelpers.ConvertFromIpAddressToInteger(ipStr);
                
                intIp.Should().BeGreaterThanOrEqualTo(ipRange.Value.IpFrom,
                    $"actual {ipStr} less than {IpHelpers.ConvertFromIntegerToIpAddress(ipRange.Value.IpFrom)}");
                
                intIp.Should().BeLessThanOrEqualTo(ipRange.Value.IpTo,
                    $"actual {ipStr} greater than {IpHelpers.ConvertFromIntegerToIpAddress(ipRange.Value.IpTo)}");
            }
        }
    }
}

﻿using BykovD.TestTask.MQ.Geo.Domain.Searchers;
using BykovD.TestTask.MQ.Geo.Tests.Helpers;
using FluentAssertions;
using Xunit;

namespace BykovD.TestTask.MQ.Geo.Tests.DataTests
{
    public class RangeBinarySearcher_Tests
    {
        [Theory]
        [InlineData(1, null, null, new int[] { })]
        [InlineData(1, null, null, new[] { 3, 5 })]
        [InlineData(1, 0, 1, new [] { 1, 1 })]
        [InlineData(1, 0, 2, new [] { 1, 1, 1 })]
        [InlineData(5, 0, 0, new[] { 5, 7, 7, 9 })]
        [InlineData(5, 0, 1, new [] { 5, 5, 7, 9 })]
        [InlineData(5, 0, 3, new [] { 5, 5, 5, 5 })]
        [InlineData(5, 1, 3, new [] { 1, 5, 5, 5 })]
        [InlineData(5, 2, 3, new [] { 1, 2, 5, 5 })]
        [InlineData(5, 3, 3, new [] { 1, 2, 4, 5 })]
        [InlineData(5, 0, 0, new[] { 5, 6, 7, 7, 9 })]
        [InlineData(5, 0, 1, new[] { 5, 5, 7, 7, 9 })]
        [InlineData(5, 0, 2, new[] { 5, 5, 5, 7, 9 })]
        [InlineData(5, 0, 3, new[] { 5, 5, 5, 5, 9 })]
        [InlineData(5, 0, 4, new[] { 5, 5, 5, 5, 5 })]
        [InlineData(5, 1, 4, new[] { 1, 5, 5, 5, 5 })]
        [InlineData(5, 2, 4, new[] { 1, 2, 5, 5, 5 })]
        [InlineData(5, 3, 4, new[] { 1, 2, 3, 5, 5 })]
        [InlineData(5, 4, 4, new[] { 1, 2, 3, 4, 5 })]
        [InlineData(7, 0, 0, new[] { 7, 18, 29, 34, 55 })]
        [InlineData(7, 1, 1, new[] { 1, 7, 29, 34, 55 })]
        [InlineData(7, 2, 2, new[] { 1, 3, 7, 34, 55 })]
        [InlineData(7, 3, 3, new[] { 1, 3, 5, 7, 55 })]
        [InlineData(7, 4, 4, new[] { 1, 3, 5, 6, 7 })]
        [InlineData(0, null, null, new[] { 1, 3, 5, 7, 9 })]
        [InlineData(2, null, null, new[] { 1, 3, 5, 7, 9 })]
        [InlineData(4, null, null, new[] { 1, 3, 5, 7, 9 })]
        [InlineData(6, null, null, new[] { 1, 3, 5, 7, 9 })]
        [InlineData(8, null, null, new[] { 1, 3, 5, 7, 9 })]
        [InlineData(10, null, null, new[] { 1, 3, 5, 7, 9 })]
        [InlineData(0, null, null, new[] { 1, 3, 5, 7 })]
        [InlineData(2, null, null, new[] { 1, 3, 5, 7 })]
        [InlineData(4, null, null, new[] { 1, 3, 5, 7 })]
        [InlineData(6, null, null, new[] { 1, 3, 5, 7 })]
        [InlineData(8, null, null, new[] { 1, 3, 5, 7 })]
        public void Test_Int_Search(int search, int? start, int? end, int[] array)
        {
            var comparer = new ComparableSourceComparer<int>(search);
            var navigator = new ArrayNavigator<int>(array);
            var searcher = new RangeBinarySearcher<int>();

            var range = searcher.Find(comparer, navigator);

            if(range == null)
            {
                (null as int?).Should().Be(start);
                (null as int?).Should().Be(end);
            }
            else
            {
                range.Value.Start.Should().Be(start);
                range.Value.End.Should().Be(end);
            }
        }
    }
}

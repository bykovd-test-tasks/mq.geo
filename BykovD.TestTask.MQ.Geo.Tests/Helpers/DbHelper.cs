﻿using BykovD.TestTask.MQ.Geo.Data;
using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace BykovD.TestTask.MQ.Geo.Tests.Helpers
{
    public static class DbHelper
    {
        public static IConfigurationRoot GetConfiguration()
        {
            return new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile(@"appsettings.json", false, false)
               .Build();
        }

        public static async Task<DbModel> GetDbModelAsync()
        {
            var dbLoader = new DbLoader(GetConfiguration());

            return await dbLoader.GetDbModel();
        }

        public static DbModel GetDbModel()
        {
            var dbLoader = new DbLoader(GetConfiguration());

            return dbLoader.GetDbModel().Result;
        }

        public static Dictionary<string, int> GetCityNameToCountDict(Span<Location> span)
        {
            Dictionary<string, int> dict = new Dictionary<string, int>(span.Length);
            Location loc;

            unsafe
            {
                for (var i = 0; i < span.Length; i++)
                {
                    loc = span[i];

                    var str = Encoding.UTF8.GetString(new ReadOnlySpan<byte>(loc.City, 24)).TrimEnd('\0');

                    if (!dict.ContainsKey(str))
                        dict.Add(str, 1);
                    else
                        dict[str] = dict[str] + 1;
                }
            }

            return dict;
        }
    }
}

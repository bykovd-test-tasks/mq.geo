﻿using BykovD.TestTask.MQ.Geo.Domain;
using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Models;
using System.Collections.Generic;
using System.Linq;

namespace BykovD.TestTask.MQ.Geo.Tests.Helpers
{

    public static class IpRangeSources
    {
        static Dictionary<int, List<IpRangesStrings>> dict = new Dictionary<int, List<IpRangesStrings>>()
        {
            {
                1,
                new List<IpRangesStrings>() {
                    new IpRangesStrings { IpFrom = "0.0.0.0", IpTo = "0.0.0.122" },
                    new IpRangesStrings { IpFrom = "0.0.0.123", IpTo = "0.0.0.255" },
                    new IpRangesStrings { IpFrom = "0.0.1.0", IpTo = "0.0.1.255" },
                    new IpRangesStrings { IpFrom = "0.0.3.0", IpTo = "0.0.3.128" },
                    new IpRangesStrings { IpFrom = "0.0.3.129", IpTo = "0.0.3.255" },
                    new IpRangesStrings { IpFrom = "0.0.4.10", IpTo = "0.0.4.255" },
                }
            },
        };

        public static IpRange[] Get(int sourceId)
        {
            var strList = dict[sourceId];

            return strList
                .Select(str => new IpRange()
                {
                    IpFrom = IpHelpers.ConvertFromIpAddressToInteger(str.IpFrom),
                    IpTo = IpHelpers.ConvertFromIpAddressToInteger(str.IpTo)
                })
                .ToArray();
        }
    }

    internal record IpRangesStrings
    {
        public string IpFrom { get; init; }

        public string IpTo { get; init; }
    }
}

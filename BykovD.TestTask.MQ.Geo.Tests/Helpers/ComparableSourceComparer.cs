﻿using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Services;
using System;

namespace BykovD.TestTask.MQ.Geo.Tests.Helpers
{
    public class ComparableSourceComparer<T> : ISourceComparer<T> where T : IComparable<T>
    {
        T source;

        public ComparableSourceComparer(T source)
        {
            this.source = source;
        }

        public int Compare(T item)
        {
            return source.CompareTo(item);
        }
    }
}

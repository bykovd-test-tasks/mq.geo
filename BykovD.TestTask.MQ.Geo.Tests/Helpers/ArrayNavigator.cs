﻿using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Services;

namespace BykovD.TestTask.MQ.Geo.Tests.Helpers
{
    internal class ArrayNavigator<T> : INavigator<T>
    {
        T[] array;

        public ArrayNavigator(T[] array)
        {
            this.array = array;
        }

        public T Get(int i)
        {
            return array[i];
        }

        public int Length()
        {
            return array.Length;
        }
    }
}

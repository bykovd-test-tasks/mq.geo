﻿using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Models;
using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Services;
using System.Runtime.InteropServices;

namespace BykovD.TestTask.MQ.Geo.Domain.Navigators
{
    public class LocationByCityIndexNavigator : INavigator<Location>
    {
        byte[] locationBytes;
        byte[] cityIndexBytes;
        int length;

        public LocationByCityIndexNavigator(byte[] locationBytes, byte[] cityIndexBytes)
        {
            this.locationBytes = locationBytes;
            this.cityIndexBytes = cityIndexBytes;
            length = cityIndexBytes.Length / 4;
        }

        public Location Get(int cityIndex)
        {
            var cityIndexSpan = MemoryMarshal.Cast<byte, int>(cityIndexBytes);

            var cityIndexValue = cityIndexSpan[cityIndex];

            var locationIndex = cityIndexValue / 96;

            var locationSpan = MemoryMarshal.Cast<byte, Location>(locationBytes);
            
            return locationSpan[locationIndex];
        }

        public int Length()
        {
            return length;
        }
    }
}

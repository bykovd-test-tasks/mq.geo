﻿using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Models;
using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Services;
using System.Runtime.InteropServices;

namespace BykovD.TestTask.MQ.Geo.Domain.Navigators
{
    public class IpRangeNavigator : INavigator<IpRange>
    {
        byte[] bytes;

        public IpRangeNavigator(byte[] bytes)
        {
            this.bytes = bytes;
        }

        public IpRange Get(int i)
        {
            var span = MemoryMarshal.Cast<byte, IpRange>(bytes);

            return span[i];
        }

        public int Length()
        {
            var span = MemoryMarshal.Cast<byte, IpRange>(bytes);

            return span.Length;
        }
    }
}

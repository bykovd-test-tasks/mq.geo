﻿using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Models;
using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Services;
using System.Text;

namespace BykovD.TestTask.MQ.Geo.Domain.Searchers
{
    public class RangeBinarySearcher<T>
    {
        public (int Start, int End)? Find(ISourceComparer<T> comparator, INavigator<T> navigator)
        {
            if (navigator.Length() == 0)
                return null;

            var start = FindStart(comparator, navigator, 0, navigator.Length() - 1, false);

            if (start == -1)
                return null;

            var end = FindEnd(comparator, navigator, start, navigator.Length() - 1, false);
            
            return (start, end);
        }


        private int FindStart(ISourceComparer<T> comparator, INavigator<T> navigator, int startIndex, int endIndex, bool wasFound)
        {
            var middleIndex = startIndex + (endIndex - startIndex) / 2;

            var entity = navigator.Get(middleIndex);

#if DEBUG
            var cityName = CityString(entity as Location?);
#endif

            var compareRes = comparator.Compare(entity);

            if (startIndex == endIndex)
            {
                if (compareRes == 0)
                    return startIndex;
                else if (wasFound)
                    return startIndex + 1;
                else
                    return -1;
            }

            if (compareRes <= 0)
            {
                if (compareRes == 0)
                    wasFound = true;

                return FindStart(comparator, navigator, startIndex, middleIndex, wasFound);
            }
            else
            {
                if (startIndex == middleIndex)
                    middleIndex++;

                return FindStart(comparator, navigator, middleIndex, endIndex, wasFound);
            }
        }

        private int FindEnd(ISourceComparer<T> comparator, INavigator<T> navigator, int startIndex, int endIndex, bool wasFound)
        {
            var middleIndex = startIndex + (endIndex - startIndex) / 2;

            var entity = navigator.Get(middleIndex);

#if DEBUG
            var cityName = CityString(entity as Location?);
#endif

            var compareRes = comparator.Compare(entity);

            if (startIndex == endIndex)
            {
                if (compareRes == 0)
                    return startIndex;
                else if (wasFound)
                    return startIndex - 1;
                else 
                    return -1;
            }

            if (compareRes < 0)
                return FindEnd(comparator, navigator, startIndex, middleIndex, wasFound);
            else
            {
                if (compareRes == 0)
                    wasFound = true;

                if (startIndex == middleIndex)
                    middleIndex++;

                return FindEnd(comparator, navigator, middleIndex, endIndex, wasFound);
            }
        }

        public string? CityString(Location? location)
        {
            if(location == null)
                return null;

            var loc = location.Value;

            unsafe
            {
                return Encoding.ASCII.GetString(new ReadOnlySpan<byte>(loc.City, 24));
            }
        }
    }
}

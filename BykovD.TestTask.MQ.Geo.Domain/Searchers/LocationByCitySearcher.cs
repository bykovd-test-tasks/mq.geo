﻿using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Models;
using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Services;
using BykovD.TestTask.MQ.Geo.Domain.Comparers;
using BykovD.TestTask.MQ.Geo.Domain.Navigators;
using System.Runtime.InteropServices;
using System.Text;

namespace BykovD.TestTask.MQ.Geo.Domain.Searchers
{
    public class LocationByCitySearcher
    {
        IDbLoader dbLoader;
        RangeBinarySearcher<Location> locationBinaryRangeSearcher = new RangeBinarySearcher<Location>();

        public LocationByCitySearcher(IDbLoader dbLoader)
        {
            this.dbLoader = dbLoader;
        }

        public async Task<Location[]> FindLocations(string searchName)
        {
            var dbModel = await dbLoader.GetDbModel();

            return FindLocations(searchName, dbModel);
        }

        private Location[] FindLocations(string searchName, DbModel dbModel)
        {
            var cityNameBytes = Encoding.ASCII.GetBytes(searchName);

            var comparator = new LocationByCityComparer(cityNameBytes);

            var navigator = new LocationByCityIndexNavigator(dbModel.LocationBytes, dbModel.NameIndexes);

            var range = locationBinaryRangeSearcher.Find(comparator, navigator);

            if (range == null)
                return Array.Empty<Location>();
            
            var res = new Location[range.Value.End - range.Value.Start + 1];

            for(int i = 0; range.Value.Start + i <= range.Value.End; i++)
                res[i] = navigator.Get(range.Value.Start + i);   

            return res;
        }

        public async Task<Location> GetLocation(int locationIndex)
        { 
            var dbModel = await dbLoader.GetDbModel();

            return GetLocation(locationIndex, dbModel);
        }

        private Location GetLocation(int locationIndex, DbModel dbModel)
        {
            var span = MemoryMarshal.Cast<byte, Location>(dbModel.LocationBytes);

            return span[locationIndex];
        }
    }
}

﻿using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Services;

namespace BykovD.TestTask.MQ.Geo.Domain.Searchers
{
    public class BinarySearcher<T>
    {
        public int Find(ISourceComparer<T> comparator, INavigator<T> navigator)
        {
            if (navigator.Length() == 0)
                return -1;

            return Find(comparator, navigator, 0, navigator.Length() - 1);
        }

        private int Find(ISourceComparer<T> comparator, INavigator<T> navigator, int startIndex, int endIndex)
        {
            if (startIndex == endIndex)
            {
                if (comparator.Compare(navigator.Get(startIndex)) == 0)
                    return startIndex;
                else
                    return -1;
            }

            var middleIndex = startIndex + (endIndex - startIndex) / 2;

            var entity = navigator.Get(middleIndex);

            var compareRes = comparator.Compare(entity);

            if (compareRes < 0)
                return Find(comparator, navigator, startIndex, middleIndex);
            else if (compareRes == 0)
                return middleIndex;
            else
            {
                if (middleIndex == startIndex)
                    middleIndex++;

                return Find(comparator, navigator, middleIndex, endIndex);
            }
        }
    }
}

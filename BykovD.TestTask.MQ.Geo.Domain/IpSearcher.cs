﻿using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Models;
using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Services;
using BykovD.TestTask.MQ.Geo.Domain.Comparers;
using BykovD.TestTask.MQ.Geo.Domain.Navigators;
using BykovD.TestTask.MQ.Geo.Domain.Searchers;

namespace BykovD.TestTask.MQ.Geo.Domain
{
    public class IpSearcher
    {
        IDbLoader dbLoader;
        BinarySearcher<IpRange> binarySearcher = new BinarySearcher<IpRange>();
        
        public IpSearcher(IDbLoader dbLoader)
        {
            this.dbLoader = dbLoader;
        }

        public async Task<IpRange?> FindIpRange(string ip)
        {
            var dbModel = await dbLoader.GetDbModel();

            return FindIpRange(ip, dbModel);
        }

        private IpRange? FindIpRange(string ip, DbModel dbModel)
        {
            var ipUint = IpHelpers.ConvertFromIpAddressToInteger(ip);

            var comparator = new IpInRangeComparer(ipUint);

            var navigator = new IpRangeNavigator(dbModel.IpRangesBytes);

            var index = binarySearcher.Find(comparator, navigator);
      
            if (index == -1)
                return null;

            return navigator.Get(index);
        }
    }
}

﻿using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Models;
using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Services;

namespace BykovD.TestTask.MQ.Geo.Domain.Comparers
{
    public class LocationByCityComparer : ISourceComparer<Location>
    {
        byte[] cityNameBytes;

        public LocationByCityComparer(byte[] cityNameBytes)
        {
            this.cityNameBytes = cityNameBytes;
        }

        public int Compare(Location location)
        {
            unsafe
            {
                var citySpan = new ReadOnlySpan<byte>(location.City, 24);

                for (int i = 0; i < Math.Max(cityNameBytes.Length, citySpan.Length); i++)
                {
                    if (i < Math.Min(cityNameBytes.Length, citySpan.Length))
                    {
                        var res = cityNameBytes[i].CompareTo(citySpan[i]);
                        if (res != 0)
                            return res;
                    }
                    else if (i < cityNameBytes.Length)
                    {
                        if (cityNameBytes[i] != '\0')
                            return 1;
                    }
                    else if (i < citySpan.Length)
                    {
                        if (citySpan[i] != '\0')
                            return -1;
                    }
                }
            }

            return 0;
        }
    }
}

﻿using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Models;
using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Services;

namespace BykovD.TestTask.MQ.Geo.Domain.Comparers
{
    internal class IpInRangeComparer : ISourceComparer<IpRange>
    {
        uint ip;

        public IpInRangeComparer(uint ip)
        {
            this.ip = ip;
        }

        public int Compare(IpRange range)
        {
            if (ip < range.IpFrom)
                return -1;
            else if (range.IpFrom <= ip && ip <= range.IpTo)
                return 0;
            else
                return 1;
        }
    }
}

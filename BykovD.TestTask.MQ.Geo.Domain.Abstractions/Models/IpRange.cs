﻿using System.Runtime.InteropServices;

namespace BykovD.TestTask.MQ.Geo.Domain.Abstractions.Models
{
    /// <summary>
    /// </summary>
    /// <param name="IpFrom">начало диапазона IP адресов</param>
    /// <param name="IpTo">конец диапазона IP адресов</param>
    /// <param name="LocationIndex">индекс записи о местоположении</param>
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public record struct IpRange(uint IpFrom, uint IpTo, uint LocationIndex)
    {
    }
}

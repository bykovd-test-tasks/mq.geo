﻿namespace BykovD.TestTask.MQ.Geo.Domain.Abstractions.Models
{
    public record DbInfo
    {
        /// <summary>
        /// версия база данных
        /// </summary>
        public int Version { get; init; }

        /// <summary>
        /// название/префикс для базы данных
        /// </summary>
        public string Name { get; init; }

        /// <summary>
        /// время создания базы данных
        /// </summary>
        public ulong Timestamp { get; init; }

        /// <summary>
        /// общее количество записей
        /// </summary>
        public int Records { get; init; }

        /// <summary>
        /// смещение относительно начала файла до начала списка записей с геоинформацией 
        /// </summary>
        public uint OffsetRanges { get; init; }

        /// <summary>
        /// смещение относительно начала файла до начала индекса с сортировкой по названию городов
        /// </summary>
        public uint OffsetCities { get; init; }

        /// <summary>
        /// смещение относительно начала файла до начала списка записей о местоположении
        /// </summary>
        public uint OffsetLocations { get; init; }
    }
}

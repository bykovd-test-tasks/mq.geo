﻿namespace BykovD.TestTask.MQ.Geo.Domain.Abstractions.Models
{
    public record DbModel
    {
        public DbInfo DbInfo { get; init; }

        public byte[] IpRangesBytes { get; init; }
        
        public byte[] LocationBytes { get; init; }
        
        public byte[] NameIndexes { get; set; }
    }
}

﻿using System.Runtime.InteropServices;
using System.Text;

namespace BykovD.TestTask.MQ.Geo.Domain.Abstractions.Models
{
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public unsafe struct Location
    {
        /// <summary>
        /// название страны (случайная строка с префиксом "cou_")
        /// </summary>
        public fixed byte Country[8];

        /// <summary>
        /// название области (случайная строка с префиксом "reg_")
        /// </summary>
        public fixed sbyte Region[12];

        /// <summary>
        /// почтовый индекс (случайная строка с префиксом "pos_")
        /// </summary>
        public fixed sbyte Postal[12];

        /// <summary>
        /// название города (случайная строка с префиксом "cit_")
        /// </summary>
        public fixed sbyte City[24];

        /// <summary>
        /// название организации (случайная строка с префиксом blic u"org_")
        /// </summary>
        public fixed sbyte Organization[32];

        /// <summary>
        /// широта
        /// </summary>
        public float Latitude;

        /// <summary>
        /// долгота
        /// </summary>
        public float Longitude;

    }
}
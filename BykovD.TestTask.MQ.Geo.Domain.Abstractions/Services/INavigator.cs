﻿namespace BykovD.TestTask.MQ.Geo.Domain.Abstractions.Services
{
    public interface INavigator<T>
    {
        T Get(int i);

        int Length();
    }
}

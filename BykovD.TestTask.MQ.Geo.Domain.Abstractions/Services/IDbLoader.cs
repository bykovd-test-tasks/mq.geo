﻿using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Models;

namespace BykovD.TestTask.MQ.Geo.Domain.Abstractions.Services
{
    public interface IDbLoader
    {
        Task<DbModel> GetDbModel();
    }
}

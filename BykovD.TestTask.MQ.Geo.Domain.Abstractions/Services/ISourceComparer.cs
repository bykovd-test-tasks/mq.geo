﻿using BykovD.TestTask.MQ.Geo.Domain.Abstractions.Models;

namespace BykovD.TestTask.MQ.Geo.Domain.Abstractions.Services
{
    public interface ISourceComparer<T>
    {
        int Compare(T item);
    }
}
